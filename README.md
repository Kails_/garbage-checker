## Index

* [Description](#Description)
* [Installation](#installation)
* [Usage](#usage)
* [License](#license)
* [Acknowledgements](#Acknowledgements)

## Description
Bash script that help the user mantain a tidy directory environment

### Main Goal
Remove folders and files of unuseful/deleted programs

### How does it work
It compares elements (dirs/files) names in a specific path with the
names present in a file (reference), if the path and the reference contain
different elements tells the user

## Installation

### Requirements
* [trash-cli](https://github.com/andreafrancia/trash-cli)

```sh
chmod 774 install.sh uninstall.sh && ./install.sh
```
## Usage
```sh
garbage-checker
```
### How to config
1. Modify the file ```$HOME/.config/garbage-checker/config```, every line is an entry
and shoud respect the format ```<path> <reference>```, the two strings must be divided
by a space, ```<reference>``` string must be a name only and not a path,
```<path>``` string must be an absolute path, and cannot handle names with spaces.

2. Execute the script once and confirm to update the refs

## License
Distributed under the ```BSD 2-Clause "Simplified"``` license.

## Acknowledgements

* [ANSI Escape Sequences](https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797)
