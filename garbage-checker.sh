#!/bin/bash

#modified ls, needed for string parsing
lsm="ls -a"

#ANSI color codes
reset="\\033[0m"
cyan="\\033[96m"
red_bk="\\033[41m"
red="\\033[91m"
green="\\033[32m"

configpath=$HOME/.config/garbage-checker

#paths array from config file
paths=()
#for every first string of every line of config file
for i in $(awk "{ print $ 1 }" "$configpath/config"); do
	#add the string to array
	paths+=($i)
done

#refs array from config file
refs=()
#for every second string of every line of config file
for i in $(awk "{ print $ 2 }" "$configpath/config"); do
	#prefix it with the config path and add it to array
	refs+=("$configpath/$i")
done

#refs updater
printf \
"${red_bk}Do you want to update the references?${reset}${red} [y/n]${reset}"

read -r choice
#if choice is y or Y or empty
if [[ "$choice" =~ [yY]{1} || "$choice" == "" ]]; then

	#for every index of paths
	for i in ${!paths[@]}; do
		#execute and redirect the output on the corrispective reference
		$lsm ${paths[$i]} > ${refs[$i]}

	done

	printf "${green}DONE${reset}\n"
fi
printf "\n"

#checker
for i in ${!paths[@]}; do

	printf \
	"${cyan}Checking   <${paths[$i]}>\nreference: <${refs[$i]}>${reset}\n"

	#aquires paths and references elements (dir/files names)
	elems=( $($lsm "${paths[$i]}") )
	elemsref=( $(cat "${refs[$i]}") )

	#for every index of elems
	for j in ${!elems[@]}; do

		#if path element is not in reference array
		if [[ ! " ${elemsref[*]} " =~ " ${elems[$j]} " ]]; then
    			printf "${red_bk}${elems[$j]}${reset}${red} not present in \
				reference, delete? [y/n]${reset}"
			read -r choice

			if [[ "$choice" =~ [yY]{1} || "$choice" == "" ]]; then
				trash-put \
				${paths[$i]}/${elems[$j]} || rm -r ${paths[$i]}/${elems[$j]}
			fi
		fi

	done

	printf "${green}DONE${reset}\n\n"

done
